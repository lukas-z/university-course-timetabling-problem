# University Course Timetabling Problem

A collection of scripts to show the use of a mathematical problem solver
to calculate a timetable for a school or university.

## Prerequisites

This project is developed using **Python 3.7** and **Gurobi**.
A license is required to run Gurobi (free Academic license available).
Additionally, some packages need to be installed through pip:
```shell
pip install -r requirements.txt
```

## Setting up the database

Before running any scripts a database needs to be set up.
I used postgres but any sql database should work.
Steps:
* set up database
* adjust connection string in `data/Database.py`
* use the scripts in the `sql` directory:
  + run `room_types.sql`
  + run `schema.sql`
  + (optional) adjust values for settings in `settings.sql`
  + run `settings.sql`

The database is ready now.
Next we need to fill it with some data.

## Generating data

Run the following command to generate timeslots according to the settings in the database:
```shell
python generate_timeslots.py
```

Run the following command with the correct parameters to generate data for the optimization:
```shell
python generate_lecture_data.py --help
```
Or just use one of the examples below.

Be careful to not generate too much data!
The biggest limitations are the memory and time.
If the model get too big parts of it will be written to the disk,
which significantly reduces the speed.
Additionally, the script might fail during optimization because it runs out of memory.

### Examples

Here are some examples of data to run as well as an example
that roughly represents the numbers at the FHV.
Added are the time and memory it took for the building and optimization of the model
on a test machine as well as the problem size (size of the model).

#### 30 lecture units per week

Time: < 1 minute  
Memory: < 1 GB  
Problem size: Optimize a model with 3588 rows, 240000 columns and 3886400 nonzeros

```shell
python generate_lecture_data.py --rooms 10 --groups 5 --lecturers 5 --lectures_per_group 2
```

#### 90 lecture units per week

Time: ~ 10 minutes  
Memory: ~ 7 GB  
Problem size: Optimize a model with 7222 rows, 2880000 columns and 46809600 nonzeros

```shell
python generate_lecture_data.py --rooms 10 --groups 10 --lecturers 10 --lectures_per_group 3
```

#### 180 lecture units per week

Time: ~ 2 hours  
Memory: ~ 45 GB  
Problem size: Optimize a model with 12452 rows, 19440000 columns and 316483200 nonzeros

```shell
python generate_lecture_data.py --rooms 15 --groups 15 --lecturers 15 --lectures_per_group 4
```

#### 225 lecture units per week

Time: ~ 2 hours  
Memory: ~ 50 GB  
Problem size: Optimize a model with 14176 rows, 21600000 columns and 351432000 nonzeros

```shell
python generate_lecture_data.py --rooms 10 --groups 15 --lecturers 20 --lectures_per_group 5
```

#### FHV estimate (1800 lecture units per week)

This is not solvable by any currently available hardware.

```shell
python generate_lecture_data.py --rooms 100 --groups 150 --lecturers 200 --lectures_per_group 4
```

## Running the optimization

Before running the script check the parameters for the model in `main.py`.
The `Threads` and `NodefileStart` settings should be adjusted according to the available hardware.
All the parameters are described in more detail in the code comments.

Then run the script using
```shell
python main.py
```

After solving the problem, the script will store the result in the `planned_class` table.

### Constraints

The model runs with the following constraints:

* Room may only be used once per timeslot
* Room is large enough to hold the student group

* Student group may only attend one lecture per timeslot
* Student group has a maximum of 10 lectures per day
* Student group has a maximum of 6 consecutive lectures without a break

* Lecturer may only hold one lecture per timeslot
* Lecturer has a maximum of 10 lectures per day
* Lecturer has a maximum of 6 consecutive lectures without a break

* Lecture is not held on Saturday afternoon
* Lecture is not held more often than needed (restriction for optimization objective)
* Lecture is held in blocks (2+ consecutive lecture units) if necessary
* Lecture is held in the correct type of room
* Lecture is attended by the correct student group and held by the correct lecturer

### Resource requirements

The model built by Gurobi tends to be very big even for a
relatively small amount of variables.
Building and solving the model will require huge amounts of memory.
Solving the model might also heavily use the CPU.

Note: Every CPU thread gets its own copy of the model to work on it.
If you run into out-of-memory errors during optimization, try reducing the thread count.

## Plotting the result

To get a visualization of the planned classes you can use the `plot.py` script.
Run with the corresponding mode to get the representation grouped by
room, student group, lecturer:
```shell
python plot.py --mode=room
python plot.py --mode=group
python plot.py --mode=lecturer
```
