from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime, Boolean

Base = declarative_base()


class RoomType(Base):
    __tablename__ = 'room_type'

    id = Column(Integer, primary_key=True)
    type = Column(String)

    rooms = relationship("Room")
    lectures = relationship("Lecture")


class Room(Base):
    __tablename__ = 'room'

    id = Column(Integer, primary_key=True)
    size = Column(Integer)
    room_type_id = Column(Integer, ForeignKey('room_type.id'))

    classes = relationship("PlannedClass")


class StudentGroup(Base):
    __tablename__ = 'student_group'

    id = Column(Integer, primary_key=True)
    size = Column(Integer)

    lectures = relationship("Lecture")


class Timeslot(Base):
    __tablename__ = 'timeslot'

    id = Column(Integer, primary_key=True)
    start_time = Column(DateTime)
    end_time = Column(DateTime)

    classes = relationship("PlannedClass")


class Lecturer(Base):
    __tablename__ = 'lecturer'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    lectures = relationship("Lecture")


class Lecture(Base):
    __tablename__ = 'lecture'

    id = Column(Integer, primary_key=True)
    units_per_week = Column(Integer)
    lecture_in_blocks = Column(Boolean)
    group_id = Column(Integer, ForeignKey('student_group.id'))
    lecturer_id = Column(Integer, ForeignKey('lecturer.id'))
    room_type_id = Column(Integer, ForeignKey('room_type.id'))

    classes = relationship("PlannedClass")


class PlannedClass(Base):
    __tablename__ = 'planned_class'

    id = Column(Integer, primary_key=True)
    timeslot_id = Column(Integer, ForeignKey('timeslot.id'))
    room_id = Column(Integer, ForeignKey('room.id'))
    lecture_id = Column(Integer, ForeignKey('lecture.id'))


class Setting(Base):
    __tablename__ = 'setting'

    key = Column(String, primary_key=True)
    value = Column(String)


def get_id(entity, name='id'):
    return getattr(entity, name)


def find_by_id(entity_id, entities):
    for entity in entities:
        if entity_id == entity.id:
            return entity

    return None
