from data.database import Database
from data.entities import Setting


class SettingsService:
    def __init__(self):
        session = Database().get_new_session()
        self.__settings = session.query(Setting).all()
        session.close()

    def get(self, key):
        for setting in self.__settings:
            if setting.key == key:
                return setting.value

        return None
