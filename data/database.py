import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Database:
    __session = None

    def __init__(self):
        engine = create_engine('postgresql+psycopg2://postgres:postgres@localhost/postgres')
        self.__session = sessionmaker(bind=engine)

    def get_new_session(self):
        return self.__session()


def truncate_table(table=None):
    if table:
        if not re.compile('^[a-zA-Z_]*$').match(table):
            raise Exception("table name is invalid")
        session = Database().get_new_session()
        session.execute('TRUNCATE TABLE %s CASCADE' % table)
        # this makes sure that any new data will start with id 1 again,
        # which is important for solving the model
        session.execute('ALTER SEQUENCE %s_id_seq RESTART' % table)
        session.commit()
        session.close()
