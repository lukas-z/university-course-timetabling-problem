import atexit
from functools import reduce
from time import process_time


def seconds_to_str(time):
    return "%d:%02d:%02d.%03d" %\
           reduce(lambda ll, b: divmod(ll[0], b) + ll[1:], [(time * 1000,), 1000, 60, 60])


def log(message, elapsed=None):
    print("=" * 40)
    print(seconds_to_str(process_time()), '-', message)
    if elapsed:
        print("Elapsed time:", seconds_to_str(elapsed))
    print("=" * 40)


def endlog():
    log("End Program", process_time() - start)


start = process_time()
atexit.register(endlog)
log("Start Program")
