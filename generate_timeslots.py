from datetime import timedelta, datetime

from data.database import Database, truncate_table
from data.entities import Timeslot
from data.settings_service import SettingsService
from util import timing


def generate():
    settings = SettingsService()
    weeks_per_semester = int(settings.get('weeks_per_semester'))
    days_of_week = int(settings.get('days_of_week'))
    timeslots_per_day = int(settings.get('timeslots_per_day'))

    print("Weeks per semester: %d" % weeks_per_semester)
    print("Days of lectures per week: %d" % days_of_week)
    print("Timeslots per day: %d" % timeslots_per_day)

    # First Monday in February
    start_of_semester = datetime.strptime(settings.get('first_day_of_semester'), '%Y-%m-%d')

    # First lecture each day starts at 8:10
    earliest_lecture_start = datetime.strptime(settings.get('earliest_lecture_start'), '%H:%M').time()

    lecture_duration = timedelta(minutes=int(settings.get('lecture_duration')))
    lecture_break = timedelta(minutes=int(settings.get('lecture_break')))

    timeslots = []
    for week in range(weeks_per_semester):
        current_day = start_of_semester + timedelta(days=7 * week)

        for _ in range(days_of_week):
            current_time = datetime.combine(current_day, earliest_lecture_start)

            for timeslot in range(timeslots_per_day):
                timeslots.append(Timeslot(start_time=current_time, end_time=(current_time + lecture_duration)))

                current_time = current_time + lecture_duration + lecture_break

            current_day = current_day + timedelta(days=1)

    timing.log("Generated %d timeslots" % len(timeslots))

    truncate_table('timeslot')
    session = Database().get_new_session()

    for timeslot in timeslots:
        session.add(timeslot)

    session.commit()
    session.close()


if __name__ == "__main__":
    generate()
