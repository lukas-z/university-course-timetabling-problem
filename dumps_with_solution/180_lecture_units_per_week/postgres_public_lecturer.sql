create table lecturer
(
    id   serial
        constraint lecturer_pkey
            primary key,
    name varchar
);

alter table lecturer
    owner to postgres;

INSERT INTO public.lecturer (id, name) VALUES (1, 'Dan Anderson');
INSERT INTO public.lecturer (id, name) VALUES (2, 'William Campos');
INSERT INTO public.lecturer (id, name) VALUES (3, 'Wanda Dickerson');
INSERT INTO public.lecturer (id, name) VALUES (4, 'Susan Caldwell');
INSERT INTO public.lecturer (id, name) VALUES (5, 'Jeana Knapp');
INSERT INTO public.lecturer (id, name) VALUES (6, 'Gina Hyland');
INSERT INTO public.lecturer (id, name) VALUES (7, 'Andrea Mcmurray');
INSERT INTO public.lecturer (id, name) VALUES (8, 'Johnnie Zima');
INSERT INTO public.lecturer (id, name) VALUES (9, 'Edward Gailes');
INSERT INTO public.lecturer (id, name) VALUES (10, 'Christopher Siu');
INSERT INTO public.lecturer (id, name) VALUES (11, 'Alma Samuels');
INSERT INTO public.lecturer (id, name) VALUES (12, 'Joseph Martell');
INSERT INTO public.lecturer (id, name) VALUES (13, 'David Maples');
INSERT INTO public.lecturer (id, name) VALUES (14, 'Jerry Lachapelle');
INSERT INTO public.lecturer (id, name) VALUES (15, 'Frieda Miller');