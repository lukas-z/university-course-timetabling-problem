create table lecture
(
    id                serial
        constraint lecture_pkey
            primary key,
    units_per_week    integer,
    lecture_in_blocks boolean,
    group_id          integer
        constraint lecture_group_id_fkey
            references student_group,
    lecturer_id       integer
        constraint lecture_lecturer_id_fkey
            references lecturer,
    room_type_id      integer
        constraint lecture_room_type_id_fkey
            references room_type
);

alter table lecture
    owner to postgres;

INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (1, 3, false, 1, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (2, 2, true, 1, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (3, 4, true, 1, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (4, 3, false, 1, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (5, 2, true, 2, 5, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (6, 4, true, 2, 6, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (7, 3, false, 2, 7, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (8, 2, true, 2, 8, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (9, 4, true, 3, 9, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (10, 3, false, 3, 10, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (11, 2, true, 3, 11, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (12, 4, true, 3, 12, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (13, 3, false, 4, 13, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (14, 2, true, 4, 14, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (15, 4, true, 4, 15, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (16, 3, false, 4, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (17, 2, true, 5, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (18, 4, true, 5, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (19, 3, false, 5, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (20, 2, true, 5, 5, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (21, 4, true, 6, 6, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (22, 3, false, 6, 7, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (23, 2, true, 6, 8, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (24, 4, true, 6, 9, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (25, 3, false, 7, 10, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (26, 2, true, 7, 11, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (27, 4, true, 7, 12, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (28, 3, false, 7, 13, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (29, 2, true, 8, 14, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (30, 4, true, 8, 15, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (31, 3, false, 8, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (32, 2, true, 8, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (33, 4, true, 9, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (34, 3, false, 9, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (35, 2, true, 9, 5, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (36, 4, true, 9, 6, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (37, 3, false, 10, 7, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (38, 2, true, 10, 8, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (39, 4, true, 10, 9, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (40, 3, false, 10, 10, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (41, 2, true, 11, 11, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (42, 4, true, 11, 12, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (43, 3, false, 11, 13, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (44, 2, true, 11, 14, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (45, 4, true, 12, 15, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (46, 3, false, 12, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (47, 2, true, 12, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (48, 4, true, 12, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (49, 3, false, 13, 4, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (50, 2, true, 13, 5, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (51, 4, true, 13, 6, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (52, 3, false, 13, 7, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (53, 2, true, 14, 8, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (54, 4, true, 14, 9, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (55, 3, false, 14, 10, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (56, 2, true, 14, 11, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (57, 4, true, 15, 12, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (58, 3, false, 15, 13, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (59, 2, true, 15, 14, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (60, 4, true, 15, 15, 2);