create table room_type
(
    id   serial
        constraint room_type_pkey
            primary key,
    type varchar
);

alter table room_type
    owner to postgres;

INSERT INTO public.room_type (id, type) VALUES (1, 'general');
INSERT INTO public.room_type (id, type) VALUES (2, 'computer lab');
INSERT INTO public.room_type (id, type) VALUES (3, 'presentation room');