create table room
(
    id           serial
        constraint room_pkey
            primary key,
    size         integer,
    room_type_id integer
        constraint room_room_type_id_fkey
            references room_type
);

alter table room
    owner to postgres;

INSERT INTO public.room (id, size, room_type_id) VALUES (1, 50, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (2, 40, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (3, 60, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (4, 50, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (5, 40, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (6, 60, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (7, 50, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (8, 40, 1);
INSERT INTO public.room (id, size, room_type_id) VALUES (9, 60, 3);
INSERT INTO public.room (id, size, room_type_id) VALUES (10, 50, 2);