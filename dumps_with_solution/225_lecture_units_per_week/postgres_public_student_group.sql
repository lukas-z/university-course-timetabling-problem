create table student_group
(
    id   serial
        constraint student_group_pkey
            primary key,
    size integer
);

alter table student_group
    owner to postgres;

INSERT INTO public.student_group (id, size) VALUES (1, 40);
INSERT INTO public.student_group (id, size) VALUES (2, 30);
INSERT INTO public.student_group (id, size) VALUES (3, 50);
INSERT INTO public.student_group (id, size) VALUES (4, 40);
INSERT INTO public.student_group (id, size) VALUES (5, 30);
INSERT INTO public.student_group (id, size) VALUES (6, 50);
INSERT INTO public.student_group (id, size) VALUES (7, 40);
INSERT INTO public.student_group (id, size) VALUES (8, 30);
INSERT INTO public.student_group (id, size) VALUES (9, 50);
INSERT INTO public.student_group (id, size) VALUES (10, 40);
INSERT INTO public.student_group (id, size) VALUES (11, 30);
INSERT INTO public.student_group (id, size) VALUES (12, 50);
INSERT INTO public.student_group (id, size) VALUES (13, 40);
INSERT INTO public.student_group (id, size) VALUES (14, 30);
INSERT INTO public.student_group (id, size) VALUES (15, 50);