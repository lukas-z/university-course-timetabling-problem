create table lecturer
(
    id   serial
        constraint lecturer_pkey
            primary key,
    name varchar
);

alter table lecturer
    owner to postgres;

INSERT INTO public.lecturer (id, name) VALUES (1, 'Chris Walls');
INSERT INTO public.lecturer (id, name) VALUES (2, 'Victor Leech');
INSERT INTO public.lecturer (id, name) VALUES (3, 'Leah North');
INSERT INTO public.lecturer (id, name) VALUES (4, 'Ryan Weimer');
INSERT INTO public.lecturer (id, name) VALUES (5, 'John Vandenbosch');
INSERT INTO public.lecturer (id, name) VALUES (6, 'Gussie Kraham');
INSERT INTO public.lecturer (id, name) VALUES (7, 'Carlos Marsh');
INSERT INTO public.lecturer (id, name) VALUES (8, 'Nancy Gartin');
INSERT INTO public.lecturer (id, name) VALUES (9, 'Patricia Shah');
INSERT INTO public.lecturer (id, name) VALUES (10, 'Karen Taylor');
INSERT INTO public.lecturer (id, name) VALUES (11, 'Mary Davis');
INSERT INTO public.lecturer (id, name) VALUES (12, 'Keith Holt');
INSERT INTO public.lecturer (id, name) VALUES (13, 'Aurelia Miller');
INSERT INTO public.lecturer (id, name) VALUES (14, 'Joshua Free');
INSERT INTO public.lecturer (id, name) VALUES (15, 'Dan Jones');
INSERT INTO public.lecturer (id, name) VALUES (16, 'Jerry Ray');
INSERT INTO public.lecturer (id, name) VALUES (17, 'Sarah Derouen');
INSERT INTO public.lecturer (id, name) VALUES (18, 'Diana Wiltshire');
INSERT INTO public.lecturer (id, name) VALUES (19, 'Patricia Weymouth');
INSERT INTO public.lecturer (id, name) VALUES (20, 'Anthony Vil');