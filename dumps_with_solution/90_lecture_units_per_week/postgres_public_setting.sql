create table setting
(
    key   varchar not null
        constraint setting_pkey
            primary key,
    value varchar
);

alter table setting
    owner to postgres;

INSERT INTO public.setting (key, value) VALUES ('weeks_per_semester', '26');
INSERT INTO public.setting (key, value) VALUES ('days_of_week', '6');
INSERT INTO public.setting (key, value) VALUES ('timeslots_per_day', '16');
INSERT INTO public.setting (key, value) VALUES ('first_day_of_semester', '2021-02-01');
INSERT INTO public.setting (key, value) VALUES ('earliest_lecture_start', '08:10');
INSERT INTO public.setting (key, value) VALUES ('lecture_duration', '45');
INSERT INTO public.setting (key, value) VALUES ('lecture_break', '5');