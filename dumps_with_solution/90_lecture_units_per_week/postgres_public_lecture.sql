create table lecture
(
    id                serial
        constraint lecture_pkey
            primary key,
    units_per_week    integer,
    lecture_in_blocks boolean,
    group_id          integer
        constraint lecture_group_id_fkey
            references student_group,
    lecturer_id       integer
        constraint lecture_lecturer_id_fkey
            references lecturer,
    room_type_id      integer
        constraint lecture_room_type_id_fkey
            references room_type
);

alter table lecture
    owner to postgres;

INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (1, 3, false, 1, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (2, 2, true, 1, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (3, 4, true, 1, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (4, 3, false, 2, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (5, 2, true, 2, 5, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (6, 4, true, 2, 6, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (7, 3, false, 3, 7, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (8, 2, true, 3, 8, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (9, 4, true, 3, 9, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (10, 3, false, 4, 10, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (11, 2, true, 4, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (12, 4, true, 4, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (13, 3, false, 5, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (14, 2, true, 5, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (15, 4, true, 5, 5, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (16, 3, false, 6, 6, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (17, 2, true, 6, 7, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (18, 4, true, 6, 8, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (19, 3, false, 7, 9, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (20, 2, true, 7, 10, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (21, 4, true, 7, 1, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (22, 3, false, 8, 2, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (23, 2, true, 8, 3, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (24, 4, true, 8, 4, 1);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (25, 3, false, 9, 5, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (26, 2, true, 9, 6, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (27, 4, true, 9, 7, 3);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (28, 3, false, 10, 8, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (29, 2, true, 10, 9, 2);
INSERT INTO public.lecture (id, units_per_week, lecture_in_blocks, group_id, lecturer_id, room_type_id) VALUES (30, 4, true, 10, 10, 2);