========================================
0:00:00.453 - Start Program
========================================
========================================
0:00:00.562 - Start building model
========================================
Academic license - for non-commercial use only - expires 2021-10-20
Using license file C:\Users\Lukas\gurobi.lic
Changed value of parameter Threads to 1
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter Heuristics to 0.0
   Prev: 0.05  Min: 0.0  Max: 1.0  Default: 0.05
========================================
0:00:00.578 - Adding variables
========================================
========================================
0:02:18.765 - Setting Objective: plan as many lectures as possible
========================================
========================================
0:02:27.171 - Adding Constraints
========================================
========================================
0:02:27.171 - Room may only be used once per timeslot
========================================
========================================
0:02:43.609 - Room is large enough to hold the student group
========================================
========================================
0:02:57.390 - Student group may only attend one lecture per timeslot
========================================
========================================
0:03:15.375 - Student group has a maximum of 10 lectures per day
========================================
========================================
0:03:33.203 - Student group has a maximum of 6 consecutive lectures without a break
========================================
========================================
0:03:52.328 - Lecturer may only hold one lecture per timeslot
========================================
========================================
0:04:09.562 - Lecturer has a maximum of 10 lectures per day
========================================
========================================
0:04:26.500 - Lecturer has a maximum of 6 consecutive lectures without a break
========================================
========================================
0:04:41.609 - Lecture is not held on Saturday afternoon
========================================
========================================
0:04:56.234 - Lecture is not held more often than needed
========================================
========================================
0:05:13.062 - Lecture is held in blocks (2+ consecutive lecture units) if necessary
========================================
========================================
0:07:38.375 - Lecture is held in the correct type of room
========================================
========================================
0:07:54.578 - Lecture is attended by the correct student group and held by the correct lecturer
========================================
========================================
0:08:19.703 - Finished building model
Elapsed time: 0:08:19.140
========================================
========================================
0:08:19.703 - Start optimizing model
========================================
Gurobi Optimizer version 9.1.2 build v9.1.2rc0 (win64)
Thread count: 6 physical cores, 12 logical processors, using up to 1 threads
Optimize a model with 7222 rows, 2880000 columns and 46809600 nonzeros
Model fingerprint: 0x37854092
Model has 19200 general constraints
Variable types: 0 continuous, 2880000 integer (2880000 binary)
Coefficient statistics:
  Matrix range     [1e+00, 1e+00]
  Objective range  [1e+00, 1e+00]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 1e+01]
Presolve removed 0 rows and 0 columns (presolve time = 6s) ...
Presolve removed 8930 rows and 2860980 columns (presolve time = 16s) ...
Presolve removed 12228 rows and 2872800 columns (presolve time = 20s) ...
Presolve added 6972 rows and 0 columns
Presolve removed 0 rows and 2864160 columns
Presolve time: 23.68s
Presolved: 14194 rows, 15840 columns, 376240 nonzeros
Variable types: 0 continuous, 15840 integer (15840 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    8.8000000e+02   8.783000e+03   0.000000e+00     24s
    2731    9.0000000e+01   0.000000e+00   0.000000e+00     25s

Root relaxation: objective 9.000000e+01, 2731 iterations, 0.38 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

H    0     0                      90.0000000 15840.0000      -     -   24s
     0     0          -    0        90.00000   90.00000  0.00%     -   24s

Explored 0 nodes (3014 simplex iterations) in 25.05 seconds
Thread count was 1 (of 12 available processors)

Solution count 1: 90

Optimal solution found (tolerance 1.00e-04)
Best objective 9.000000000000e+01, best bound 9.000000000000e+01, gap 0.0000%
========================================
0:08:50.062 - Finished optimizing model
Elapsed time: 0:00:30.359
========================================
========================================
0:09:01.093 - Obj: 90
========================================
Current memory usage is 6.956542.1GB; Peak was 7.117374.1GB
========================================
0:09:06.703 - Saving 90 planned classes
========================================
========================================
0:09:11.609 - End Program
Elapsed time: 0:09:11.156
========================================

Process finished with exit code 0
