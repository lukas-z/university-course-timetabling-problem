create table lecturer
(
    id   serial
        constraint lecturer_pkey
            primary key,
    name varchar
);

alter table lecturer
    owner to postgres;

INSERT INTO public.lecturer (id, name) VALUES (1, 'Helen Murphy');
INSERT INTO public.lecturer (id, name) VALUES (2, 'Mark Woodman');
INSERT INTO public.lecturer (id, name) VALUES (3, 'Marcus Agosto');
INSERT INTO public.lecturer (id, name) VALUES (4, 'Ramona Stern');
INSERT INTO public.lecturer (id, name) VALUES (5, 'James Craig');
INSERT INTO public.lecturer (id, name) VALUES (6, 'Albert Cohen');
INSERT INTO public.lecturer (id, name) VALUES (7, 'Edward Taylor');
INSERT INTO public.lecturer (id, name) VALUES (8, 'Jane Clayton');
INSERT INTO public.lecturer (id, name) VALUES (9, 'Karen Zielinski');
INSERT INTO public.lecturer (id, name) VALUES (10, 'Mack Whittington');