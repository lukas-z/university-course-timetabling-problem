create table planned_class
(
    id          serial
        constraint planned_class_pkey
            primary key,
    timeslot_id integer
        constraint planned_class_timeslot_id_fkey
            references timeslot,
    room_id     integer
        constraint planned_class_room_id_fkey
            references room,
    lecture_id  integer
        constraint planned_class_lecture_id_fkey
            references lecture
);

alter table planned_class
    owner to postgres;

INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (1, 1, 2, 14);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (2, 2, 1, 14);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (3, 5, 6, 13);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (4, 9, 3, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (5, 10, 3, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (6, 11, 1, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (7, 11, 4, 24);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (8, 12, 6, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (9, 12, 7, 24);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (10, 17, 4, 18);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (11, 18, 4, 18);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (12, 20, 7, 19);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (13, 21, 1, 13);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (14, 22, 7, 22);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (15, 25, 7, 19);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (16, 27, 7, 19);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (17, 31, 8, 15);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (18, 32, 7, 15);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (19, 35, 1, 22);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (20, 37, 9, 25);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (21, 38, 4, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (22, 39, 1, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (23, 39, 8, 2);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (24, 40, 3, 2);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (25, 41, 5, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (26, 42, 1, 18);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (27, 42, 5, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (28, 42, 7, 24);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (29, 43, 1, 24);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (30, 43, 5, 11);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (31, 43, 6, 18);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (32, 44, 5, 11);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (33, 45, 2, 13);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (34, 47, 6, 12);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (35, 48, 8, 12);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (36, 49, 10, 30);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (37, 50, 1, 21);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (38, 50, 10, 30);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (39, 51, 4, 21);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (40, 52, 2, 21);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (41, 52, 7, 23);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (42, 53, 1, 21);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (43, 53, 8, 23);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (44, 54, 10, 30);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (45, 55, 5, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (46, 55, 6, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (47, 55, 10, 30);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (48, 56, 4, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (49, 56, 6, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (50, 56, 10, 28);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (51, 57, 5, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (52, 57, 6, 5);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (53, 58, 2, 5);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (54, 58, 3, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (55, 59, 3, 15);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (56, 59, 10, 28);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (57, 60, 5, 15);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (58, 63, 8, 20);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (59, 64, 1, 22);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (60, 64, 8, 20);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (61, 65, 1, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (62, 65, 9, 27);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (63, 66, 9, 27);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (64, 68, 1, 16);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (65, 69, 2, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (66, 70, 9, 27);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (67, 71, 2, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (68, 71, 9, 27);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (69, 72, 8, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (70, 73, 5, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (71, 73, 9, 25);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (72, 74, 6, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (73, 75, 1, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (74, 75, 2, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (75, 76, 6, 12);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (76, 76, 9, 26);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (77, 77, 2, 12);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (78, 77, 4, 17);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (79, 77, 9, 26);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (80, 78, 1, 17);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (81, 78, 9, 25);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (82, 78, 10, 28);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (83, 79, 10, 29);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (84, 80, 4, 16);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (85, 80, 10, 29);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (86, 81, 4, 8);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (87, 82, 7, 8);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (88, 87, 3, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (89, 88, 1, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (90, 88, 4, 16);