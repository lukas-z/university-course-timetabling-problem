create table lecturer
(
    id   serial
        constraint lecturer_pkey
            primary key,
    name varchar
);

alter table lecturer
    owner to postgres;

INSERT INTO public.lecturer (id, name) VALUES (1, 'Caroline Weinstein');
INSERT INTO public.lecturer (id, name) VALUES (2, 'Pauline Moye');
INSERT INTO public.lecturer (id, name) VALUES (3, 'John Herrera');
INSERT INTO public.lecturer (id, name) VALUES (4, 'Louise Bolan');
INSERT INTO public.lecturer (id, name) VALUES (5, 'David Morgan');