create table planned_class
(
    id          serial
        constraint planned_class_pkey
            primary key,
    timeslot_id integer
        constraint planned_class_timeslot_id_fkey
            references timeslot,
    room_id     integer
        constraint planned_class_room_id_fkey
            references room,
    lecture_id  integer
        constraint planned_class_lecture_id_fkey
            references lecture
);

alter table planned_class
    owner to postgres;

INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (1, 3, 7, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (2, 5, 8, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (3, 6, 9, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (4, 7, 9, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (5, 8, 7, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (6, 9, 9, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (7, 10, 9, 9);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (8, 13, 10, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (9, 14, 8, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (10, 14, 10, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (11, 15, 7, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (12, 15, 10, 10);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (13, 21, 2, 4);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (14, 30, 1, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (15, 41, 8, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (16, 42, 5, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (17, 43, 8, 3);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (18, 49, 1, 7);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (19, 51, 4, 8);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (20, 51, 7, 5);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (21, 52, 1, 5);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (22, 52, 6, 8);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (23, 56, 8, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (24, 57, 3, 2);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (25, 58, 8, 2);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (26, 59, 8, 1);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (27, 70, 7, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (28, 71, 7, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (29, 72, 7, 6);
INSERT INTO public.planned_class (id, timeslot_id, room_id, lecture_id) VALUES (30, 73, 7, 6);