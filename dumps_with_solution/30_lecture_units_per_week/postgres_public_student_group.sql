create table student_group
(
    id   serial
        constraint student_group_pkey
            primary key,
    size integer
);

alter table student_group
    owner to postgres;

INSERT INTO public.student_group (id, size) VALUES (1, 40);
INSERT INTO public.student_group (id, size) VALUES (2, 30);
INSERT INTO public.student_group (id, size) VALUES (3, 50);
INSERT INTO public.student_group (id, size) VALUES (4, 40);
INSERT INTO public.student_group (id, size) VALUES (5, 30);