import sys
import tracemalloc
from time import process_time

from gurobipy import Model, GRB, GurobiError

from data.database import Database, truncate_table
from data.settings_service import SettingsService
from data.entities import Room, Lecture, StudentGroup, Lecturer, Timeslot, PlannedClass, get_id
from util import timing


def main():
    # fetching resources from the database
    database = Database()
    session = database.get_new_session()
    rooms = session.query(Room).all()
    lectures = session.query(Lecture).all()
    student_groups = session.query(StudentGroup).all()
    lecturers = session.query(Lecturer).all()
    timeslots = session.query(Timeslot).all()
    session.close()

    if not (rooms and lecturers and lectures and student_groups and timeslots):
        raise RuntimeError('Please populate the database! Check the README for more information.')

    # fetching settings for timeslots
    settings = SettingsService()
    days_of_week = int(settings.get('days_of_week'))
    days = range(days_of_week)
    slots_per_day = int(settings.get('timeslots_per_day'))
    slots = range(slots_per_day)

    solution = None
    try:
        tracemalloc.start()
        start = process_time()
        timing.log('Start building model')

        model = Model('university-time-tabeling')

        # Parameters
        '''
        Write MIP nodes to disk
        (when the memory usage exceeds the specified GB)
        Default: use as much memory as possible (might make other programs unusable)
        '''
        # model.setParam('NodefileStart', 40)

        '''
        Thread count
        The thread count affects memory usage, as each thread requires a copy of the model
        Default: us all available threads (might make other programs unusable)
        '''
        model.setParam('Threads', 1)

        '''
        MIP solver focus
        0 = default
        1 = focus on finding feasible solutions quickly
        2 = focus on proving optimality
        3 = focus on moving the objective bound
        '''
        # model.setParam('MIPFocus', 1)

        '''
        Time spent in feasibility heuristics
        Determines the amount of time spent in MIP heuristics (0-1)
        '''
        model.setParam('Heuristics', 0)

        '''
        Controls the presolve level
        0 = off
        1 = conservative
        2 = aggressive
        '''
        # model.setParam('Presolve', 0)

        '''
        Global cut control
        0 = no cuts
        1 = moderate cut
        2 = aggressive cut
        3 = very aggressive cut
        '''
        # model.setParam('Cuts', 0)

        # Variables
        timing.log('Adding variables')

        assignment = model.addVars(
            list(map(get_id, rooms)),
            days,
            slots,
            list(map(get_id, lectures)),
            list(map(get_id, lecturers)),
            list(map(get_id, student_groups)),
            vtype=GRB.BINARY,
            name='assignment'
        )

        # Objective
        timing.log('Setting Objective: plan as many lectures as possible')

        model.setObjective(assignment.sum(), GRB.MAXIMIZE)

        # Constraints
        timing.log('Adding Constraints')

        timing.log('Room may only be used once per timeslot')
        for room in rooms:
            for day in days:
                for slot in slots:
                    model.addConstr(
                        assignment.sum(room.id, day, slot, '*', '*', '*') <= 1,
                        'room only used once'
                    )

        timing.log('Room is large enough to hold the student group')
        for room in rooms:
            for student_group in student_groups:
                if room.size < student_group.size:
                    model.addConstr(
                        assignment.sum(room.id, '*', '*', '*', '*', student_group.id) == 0,
                        'room needs to be big enough for student group'
                    )

        timing.log('Student group may only attend one lecture per timeslot')
        for student_group in student_groups:
            for day in days:
                for slot in slots:
                    model.addConstr(
                        assignment.sum('*', day, slot, '*', '*', student_group.id) <= 1,
                        'each student group only attends one lecture at a time'
                    )

        timing.log('Student group has a maximum of 10 lectures per day')
        for student_group in student_groups:
            for day in days:
                model.addConstr(
                    assignment.sum('*', day, '*', '*', '*', student_group.id) <= 10,
                    'maximum 10 lectures per group per day'
                )

        timing.log('Student group has a maximum of 6 consecutive lectures without a break')
        for student_group in student_groups:
            for day in days:
                for slot in slots:
                    if slot < (slots_per_day - 6):
                        model.addConstr(
                            # a student group may only be assigned to 6 out of any 7 consecutive timeslots
                            assignment.sum('*', day, range(slot, slot + 7), '*', '*', student_group.id) <= 6,
                            'maximum 6 consecutive lectures per group'
                        )

        timing.log('Lecturer may only hold one lecture per timeslot')
        for lecturer in lecturers:
            for day in days:
                for slot in slots:
                    model.addConstr(
                        assignment.sum('*', day, slot, '*', lecturer.id, '*') <= 1,
                        'lecturer only holds one lecture at a time'
                    )

        timing.log('Lecturer has a maximum of 10 lectures per day')
        for lecturer in lecturers:
            for day in days:
                model.addConstr(
                    assignment.sum('*', day, '*', '*', lecturer.id, '*') <= 10,
                    'maximum 10 lectures per lecturer per day'
                )

        timing.log('Lecturer has a maximum of 6 consecutive lectures without a break')
        for lecturer in lecturers:
            for day in days:
                for slot in slots:
                    if slot < (slots_per_day - 6):
                        model.addConstr(
                            # a lecturer may only be assigned to 6 out of any 7 consecutive timeslots
                            assignment.sum('*', day, range(slot, slot + 7), '*', lecturer.id, '*') <= 6,
                            'maximum 6 consecutive lectures per lecturer'
                        )

        timing.log('Lecture is not held on Saturday afternoon')
        model.addConstr(
            assignment.sum('*', days_of_week - 1, range(8, slots_per_day), '*', '*', '*') == 0,
            'blocking Saturday afternoon'
        )

        timing.log('Lecture is not held more often than needed')
        for lecture in lectures:
            model.addConstr(
                assignment.sum('*', '*', '*', lecture.id, '*', '*') <= lecture.units_per_week,
                'lecture units per week'
            )

        timing.log('Lecture is held in blocks (2+ consecutive lecture units) if necessary')
        for lecture in lectures:
            if lecture.lecture_in_blocks:
                for room in rooms:
                    for day in days:
                        for slot in slots:
                            # there has to be a unit of the same lecture before or after each unit
                            # using max and min to avoid going out of bounds
                            timeframe = range(max(0, slot - 1), min(slots_per_day, slot + 2))

                            model.addConstr(
                                (assignment.sum(
                                    room.id,
                                    day,
                                    slot,
                                    lecture.id,
                                    lecture.lecturer_id,
                                    lecture.group_id
                                ) == 1) >> (assignment.sum(
                                    '*', day, timeframe, lecture.id, '*', '*'
                                ) >= 2),
                                'lectures in blocks'
                            )

        timing.log('Lecture is held in the correct type of room')
        for lecture in lectures:
            for room in rooms:
                if not room.room_type_id == lecture.room_type_id:
                    model.addConstr(
                        assignment.sum(room.id, '*', '*', lecture.id, '*', '*') == 0,
                        'lecture needs to be held in correct type of room'
                    )

        timing.log('Lecture is attended by the correct student group and held by the correct lecturer')
        for lecture in lectures:
            for day in days:
                for slot in slots:
                    model.addConstr(
                        assignment.sum('*', day, slot, lecture.id, '*', '*') ==
                        assignment.sum('*', day, slot, lecture.id, lecture.lecturer_id, lecture.group_id),
                        'correct student group and lecturer for lecture'
                    )

        timing.log('Finished building model', process_time() - start)
        start = process_time()
        timing.log('Start optimizing model')

        # Optimization
        model.optimize()

        timing.log('Finished optimizing model', process_time() - start)

        # Status checking
        status = model.Status
        if status in (GRB.Status.INF_OR_UNBD, GRB.Status.INFEASIBLE, GRB.Status.UNBOUNDED):
            print('The model cannot be solved because it is infeasible or unbounded')
            sys.exit(1)

        if status != GRB.Status.OPTIMAL:
            print('Optimization was stopped with status ' + str(status))
            sys.exit(1)

        solution = model.getAttr('X', assignment)

        timing.log('Obj: %g' % model.objVal)

        current, peak = tracemalloc.get_traced_memory()
        print("Current memory usage is %f.1GB; Peak was %f.1GB" % (
            (current / 10 ** 9), (peak / 10 ** 9)
        ))
        tracemalloc.stop()

    except GurobiError as error:
        print('Error code ' + str(error.errno) + ": " + str(error))
        sys.exit(1)

    except AttributeError as error:
        print(str(error))
        print('Encountered an attribute error')
        sys.exit(1)

    # Saving solution
    if solution:
        planned_classes = []
        for day in days:
            for slot in range(slots_per_day):
                timeslot = day * slots_per_day + slot

                for room in rooms:
                    for lecture in lectures:
                        if solution[room.id, day, slot, lecture.id, lecture.lecturer_id, lecture.group_id] > 0:
                            planned_classes.append(PlannedClass(
                                timeslot_id=timeslots[timeslot].id,
                                room_id=room.id,
                                lecture_id=lecture.id
                            ))

        timing.log('Saving %d planned classes' % len(planned_classes))

        truncate_table('planned_class')
        if planned_classes:
            session = database.get_new_session()

            for obj in planned_classes:
                session.add(obj)

            session.commit()
            session.close()


if __name__ == "__main__":
    main()
