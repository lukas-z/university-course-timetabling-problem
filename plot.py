import argparse
import atexit
import random

import plotly.express as px

from data.database import Database
from data.settings_service import SettingsService
from data.entities import Timeslot, PlannedClass, Room, RoomType, find_by_id, Lecture, StudentGroup, Lecturer
from util import timing


def plot():
    mode = parse_arguments().mode
    if mode not in ['room', 'group', 'lecturer']:
        raise RuntimeError(
            'Please enter a valid mode! Valid modes are: room, group, lecture, lecturer'
        )

    settings = SettingsService()
    days_of_week = int(settings.get('days_of_week'))
    timeslots_per_day = int(settings.get('timeslots_per_day'))

    session = Database().get_new_session()
    atexit.register(session.close)
    timeslots = session.query(Timeslot).limit(days_of_week * timeslots_per_day).all()
    planned_classes = session.query(PlannedClass).all()
    lectures = session.query(Lecture).all()
    rooms = session.query(Room).all()
    room_types = session.query(RoomType).all()
    groups = session.query(StudentGroup).all()
    lecturers = session.query(Lecturer).all()

    colors = []
    for _ in lectures:
        colors.append('#%06x' % random.randint(0, 0xFFFFFF))

    classes_data = []
    for planned_class in planned_classes:
        timeslot = find_by_id(planned_class.timeslot_id, timeslots)
        room = find_by_id(planned_class.room_id, rooms)
        room_type = find_by_id(room.room_type_id, room_types)
        lecture = find_by_id(planned_class.lecture_id, lectures)

        y_label = ''
        color_label = ''
        if mode == 'room':
            y_label = 'Id: %d - Size: %d - Type: %s' % (room.id, room.size, room_type.type)
            color_label = 'Id: %d - Group id: %d - Lecturer id: %s' %\
                          (lecture.id, lecture.group_id, lecture.lecturer_id)
        if mode == 'group':
            group = find_by_id(lecture.group_id, groups)
            y_label = 'Id: %d - Size: %d' % (group.id, group.size)
            color_label = 'Id: %d - Lecturer id: %s' % (lecture.id, lecture.lecturer_id)
        if mode == 'lecturer':
            lecturer = find_by_id(lecture.lecturer_id, lecturers)
            y_label = 'Id: %d - Name: %s' % (lecturer.id, lecturer.name)
            color_label = 'Id: %d - Group id: %s' % (lecture.id, lecture.group_id)

        class_data = dict(
            y=y_label,
            z=color_label,
            start=timeslot.start_time,
            end=timeslot.end_time
        )
        classes_data.append(class_data)

    title = ''
    y_title = ''
    z_title = ''
    if mode == 'room':
        title = 'Timetable of lectures by rooms'
        y_title = 'Rooms'
        z_title = 'Lectures'
    if mode == 'group':
        title = 'Timetable of lectures by student groups'
        y_title = 'Student Groups'
        z_title = 'Lectures'
    if mode == 'lecturer':
        title = 'Timetable of lectures by lecturer'
        y_title = 'Lecturers'
        z_title = 'Lectures'

    fig = px.timeline(
        classes_data,
        x_start='start',
        x_end='end',
        y='y',
        color='z',
        color_discrete_sequence=colors,
        # color_discrete_sequence=(
        #     px.colors.qualitative.Dark24 +
        #     px.colors.qualitative.Light24 +
        #     px.colors.qualitative.Alphabet),
        title=title,
        labels=dict(y=y_title, z=z_title)
    )
    fig.update_xaxes(title='Timeline')

    fig.show()


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Plots planned classes based on rooms, groups, or lecturers'
    )
    parser.add_argument(
        '--mode', type=str, required=True,
        help='(room|group|lecturer) mode for the plot'
    )

    return parser.parse_args()


if __name__ == "__main__":
    plot()
