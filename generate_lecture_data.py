import argparse

import names

from data.database import Database, truncate_table
from data.entities import Room, StudentGroup, Lecturer, Lecture
from util import timing


def generate():
    args = parse_arguments()

    truncate_table('room')
    truncate_table('student_group')
    truncate_table('lecturer')
    truncate_table('lecture')

    rooms = generate_rooms(args.rooms, args.room_size)
    student_groups = generate_student_groups(args.groups, args.group_size)
    lecturers = generate_lecturers(args.lecturers)

    session = Database().get_new_session()

    for obj in rooms + student_groups + lecturers:
        session.add(obj)

    session.commit()

    lectures = generate_lectures(
        args.lectures_per_group,
        args.lecture_units_per_week,
        student_groups,
        lecturers
    )

    for obj in lectures:
        session.add(obj)

    session.commit()
    session.close()


def generate_rooms(room_count, room_size):
    timing.log(
        "Generating %d rooms with an average capacity of %d students" %
        (room_count, room_size)
    )

    rooms = []
    for i in range(room_count):
        size = room_size
        if i % 3 == 1:
            size = room_size - 10
        if i % 3 == 2:
            size = room_size + 10

        room_type = 1
        if i / room_count >= 0.9:
            room_type = 2
        elif i / room_count >= 0.8:
            room_type = 3

        rooms.append(Room(size=size, room_type_id=room_type))

    return rooms


def generate_student_groups(group_count, group_size):
    timing.log(
        "Generating %d student groups with an average group size of %d" %
        (group_count, group_size)
    )

    groups = []
    for i in range(group_count):
        size = group_size
        if i % 3 == 1:
            size = group_size - 10
        if i % 3 == 2:
            size = group_size + 10

        groups.append(StudentGroup(size=size))

    return groups


def generate_lecturers(lecturer_count):
    timing.log("Generating %d lecturers" % lecturer_count)

    lecturers = []
    for _ in range(lecturer_count):
        lecturers.append(Lecturer(name=names.get_full_name()))

    return lecturers


def generate_lectures(
        lectures_per_group,
        lecture_units_per_week,
        student_groups,
        lecturers
):
    timing.log(
        "Generating %d lectures, %d per group; Average units per week: %d" %
        (lectures_per_group * len(student_groups), lectures_per_group, lecture_units_per_week)
    )

    lectures = []
    i = 0
    total = len(student_groups) * lectures_per_group
    for student_group in student_groups:
        for _ in range(lectures_per_group):
            units = lecture_units_per_week

            if i % 3 == 1:
                units = units - 1
            if i % 3 == 2:
                units = units + 1

            lecture_in_blocks = False
            if units % 2 == 0:
                lecture_in_blocks = True

            room_type = 1
            if i / total >= 0.9:
                room_type = 2
            elif i / total >= 0.8:
                room_type = 3

            lectures.append(Lecture(
                units_per_week=units,
                lecture_in_blocks=lecture_in_blocks,
                room_type_id=room_type,
                group_id=student_group.id,
                lecturer_id=lecturers[i % len(lecturers)].id
            ))

            i += 1

    return lectures


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Truncates tables and generates new data'
    )
    parser.add_argument(
        '--rooms', type=int, required=True,
        help='number of rooms to generate'
    )
    parser.add_argument(
        '--room_size', type=int, default=50,
        help='average room size [min: 20, default: 50]'
    )
    parser.add_argument(
        '--groups', type=int, required=True,
        help='number of student groups to generate'
    )
    parser.add_argument(
        '--group_size', type=int, default=40,
        help='average student group size [min: 20, default: 40]'
    )
    parser.add_argument(
        '--lecturers', type=int, required=True,
        help='number of lecturers to generate'
    )
    parser.add_argument(
        '--lectures_per_group', type=int, default=8,
        help='number of lectures per student group'
    )
    parser.add_argument(
        '--lecture_units_per_week',
        type=int,
        default=3,
        help='average number of units per lecture [min: 1, default: 3]'
    )

    args = parser.parse_args()

    if args.room_size < 20:
        raise argparse.ArgumentError(
            args.room_size,
            'room_size must be greater than or equal to 20'
        )
    if args.group_size < 20:
        raise argparse.ArgumentError(
            args.group_size,
            'group_size must be greater than or equal to 20'
        )
    if args.lecture_units_per_week < 1:
        raise argparse.ArgumentError(
            args.lecture_units_per_week,
            'lecture_units must be greater than or equal to 1'
        )

    return args


if __name__ == "__main__":
    generate()
