DROP TABLE IF EXISTS "setting";

CREATE TABLE "setting" (
  "key" varchar,
  "value" varchar,
  PRIMARY KEY ("key")
);

/* modify these values as needed */
INSERT INTO setting VALUES
('weeks_per_semester', 26),
('days_of_week', 6),
('timeslots_per_day', 16),
('first_day_of_semester', '2021-02-01'),
('earliest_lecture_start', '08:10'),
('lecture_duration', 45),
('lecture_break', 5);
