DROP TABLE IF EXISTS "planned_class";
DROP TABLE IF EXISTS "lecture";
DROP TABLE IF EXISTS "lecturer";
DROP TABLE IF EXISTS "student_group";
DROP TABLE IF EXISTS "timeslot";
DROP TABLE IF EXISTS "room";

CREATE TABLE "room" (
  "id" SERIAL,
  "size" int,
  "room_type_id" int,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("room_type_id") REFERENCES room_type("id")
);

CREATE TABLE "timeslot" (
  "id" SERIAL,
  "start_time" timestamp,
  "end_time" timestamp,
  PRIMARY KEY ("id")
);

CREATE TABLE "student_group" (
  "id" SERIAL,
  "size" int,
  PRIMARY KEY ("id")
);

CREATE TABLE "lecturer" (
  "id" SERIAL,
  "name" varchar,
  PRIMARY KEY ("id")
);

CREATE TABLE "lecture" (
  "id" SERIAL,
  "units_per_week" int,
  "lecture_in_blocks" bool,
  "group_id" int,
  "lecturer_id" int,
  "room_type_id" int,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("group_id") REFERENCES student_group("id"),
  FOREIGN KEY ("lecturer_id") REFERENCES lecturer("id"),
  FOREIGN KEY ("room_type_id") REFERENCES room_type("id")
);

CREATE TABLE "planned_class" (
  "id" SERIAL,
  "timeslot_id" int,
  "room_id" int,
  "lecture_id" int,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("timeslot_id") REFERENCES timeslot("id"),
  FOREIGN KEY ("room_id") REFERENCES room("id"),
  FOREIGN KEY ("lecture_id") REFERENCES lecture("id")
);
