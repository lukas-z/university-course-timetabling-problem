DROP TABLE IF EXISTS "room_type" CASCADE;

CREATE TABLE "room_type" (
  "id" SERIAL,
  "type" varchar,
  PRIMARY KEY ("id")
);

INSERT INTO room_type VALUES
(1, 'general'),
(2, 'computer lab'),
(3, 'presentation room');
